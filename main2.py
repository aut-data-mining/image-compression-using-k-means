import math
import random

import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def load_image(address):
    """ Load image """
    img = mpimg.imread(address)
    return img


def convert_3d_to_2d(img_3d):
    """
    Convert a 3d image to 2d image using make a value for rgb

    :param image_3d: 3d image
    :return: 2d image
    """
    img_2d = []
    for row in img_3d:
        new_row = []
        for point in row:
            r = point[0]
            g = point[1]
            b = point[2]

            v1 = r
            v2 = g
            new_row.append([v1, v2])
        img_2d.append(new_row)
    return img_2d


def k_means(data, k, number_iterations):
    """
    run k-means for clustering data
    :param number_iterations: number of iterations
    :param data: data that has x and y
    :return: cluster of each data, center points
    """

    x_list = []
    y_list = []
    for row in data:
        for point in row:
            x_list.append(point[0])
            y_list.append(point[1])
    c_list = []

    n = len(x_list)
    center_x = []
    center_y = []

    # initialize centers
    random_list = random.sample(range(0, n), k)

    for i in random_list:
        center_x.append(x_list[i])
        center_y.append(y_list[i])

    for iteration in range(number_iterations):
        print(iteration)
        # Find cluster of each points
        c_list = []
        for i in range(n):
            best_distance = +1000 * 1000
            best_cluster = -1
            for j in range(k):
                distance = math.hypot(x_list[i] - center_x[j], y_list[i] - center_y[j])
                if distance < best_distance:
                    best_distance = distance
                    best_cluster = j
            c_list.append(best_cluster)

        center_x = []
        center_y = []
        # Calculate new centers
        for j in range(k):
            number_points = 0
            sum_x = 0
            sum_y = 0
            for i in range(n):
                if c_list[i] == j:
                    sum_x += x_list[i]
                    sum_y += y_list[i]
                    number_points += 1

            center_x.append(sum_x / number_points)
            center_y.append(sum_y / number_points)

    return c_list, center_x, center_y


def visualize_clusters(data, c_list, k):
    """
    plot data with cluster for data

    :param c_list: list of cluster number for each data
    :param data: input data
    :param k: number of clusters
    :return: nothing
    """
    x_list = []
    y_list = []
    for row in data:
        for point in row:
            x_list.append(point[0])
            y_list.append(point[1])

    color_list = []

    random_color = random_color_list(k)

    for item in c_list:
        color_list.append(random_color[item])

    plt.scatter(x=x_list, y=y_list, color=color_list)
    plt.show()


def random_color_list(number):
    """
    Generate random list with number items
    :param number: number of items
    :return: random color list
    """

    random_list = []
    for i in range(number):
        color = (random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1))
        random_list.append(color)

    return random_list


def compress_image(img_3d, c_list):
    """
    Compress image with convert colors in a cluster to center of it
    :param img_3d: input image
    :param c_list: cluster id for every point
    :return: compressed image
    """

    k = max(c_list) + 1
    center_color = []
    cluster_size = []
    for i in range(k):
        center_color.append([0, 0, 0])
        cluster_size.append(0)

    for i, row in enumerate(img_3d):
        for j, point in enumerate(row):
            cluster_id = c_list[i*len(row)+j]
            center_color[cluster_id][0] += point[0]
            center_color[cluster_id][1] += point[1]
            center_color[cluster_id][2] += point[2]
            cluster_size[cluster_id] += 1

    for i in range(k):
        if cluster_size[i] > 0:
            center_color[i][0] /= cluster_size[i]
            center_color[i][1] /= cluster_size[i]
            center_color[i][2] /= cluster_size[i]

    img_3d_compressed = []
    for i, row in enumerate(img_3d):
        new_row = []
        for j, point in enumerate(row):
            cluster_id = c_list[i * len(row) + j]
            new_row.append(center_color[cluster_id])
        img_3d_compressed.append(new_row)
    return img_3d_compressed


if __name__ == '__main__':
    address = 'sample_img1.png'
    k = 64

    img_3d = load_image(address=address)
    img_2d = convert_3d_to_2d(img_3d)
    c_list, center_x, center_y = k_means(data=img_2d, k=k, number_iterations=3)
    visualize_clusters(data=img_2d, c_list=c_list, k=k)
    img_3d_compressed = compress_image(img_3d=img_3d, c_list=c_list)
    plt.imshow(img_3d_compressed)
    plt.show()
